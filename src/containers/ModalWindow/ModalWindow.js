import React,{Component} from 'react';
import {Modal,View,Text,StyleSheet} from "react-native";

class ModalWindow extends Component {
    state={
        modalVisible: false,
    };
    showModal(visible){
        this.setState({modalVisible :visible});
    }
    render() {
        return (
            <Modal
                animationType='fade'
                transparent={false}
                visible={this.props.modalVisible}
                onRequestClose={this.props.toggleModal}>
                <View style={styles.OrderedDishes}>
                   <Text>Order ID: {}</Text>
                </View>
                <Button
                    title='Order'
                    onPress={()=>{this.showModal(!this.showModal.modalVisible)}}
                    style={styles.btn}>
                </Button>
                <Button
                    title='Cancel'
                    onPress={()=>{this.showModal(!this.showModal.modalVisible)}}
                    style={styles.btn}>
                </Button>
            </Modal>
        );
    }
};

const styles = StyleSheet.create({
    OrderedDishes:{
        borderRadius: 1,
        borderWidth: 0.5,
        borderColor: 'grey'
    }
});

export default ModalWindow;