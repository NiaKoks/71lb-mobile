import React, {Component} from 'react';
import {StyleSheet,Text, View,Button,FlatList} from 'react-native';
import {connect} from 'react-redux'
import {addProduct, orderSuc as dishesReducer} from "../../store/actions/actions-dishes";

// import {ModalWindow} from '../ModalWindow/ModalWindow'
class MainApp extends Component {
    componentDidMount() {
        this.props.addProduct();
    }
    _renderItem = (item, index) => (
        <View key={index} className='ProductCard'>
            <Text>{item[index].title}</Text>
            <Image source={item[index].img}/>
            <Text>{item[index].price} KGS</Text>
            <Button title="Order">
            </Button>
            {/*<button onClick={()=> this.props.addToBasket(dish,this.props.dishes[dish].price)}>Order</button>*/}
        </View>
    );

    render() {
        return (
            <View style={styles.container}>
                {/*<ModalWindow/>*/}
                 <FlatList
                     renderItem={this._renderItem(this.props.dishes)}>
                 </FlatList>
                <Button
                    title="Checkout"
                    onPress={()=>{this.showModal(!this.showModal.modalVisible)}}>
                </Button>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn:{
        flex: 8,
    }

});

const mapStateToProps = state => ({
    dishes: dishesReducer.dishes
});
const mapDispatchToProps =(dispatch)=>({
    addProduct: () => dispatch(addProduct()),
});
export default connect(mapStateToProps, mapDispatchToProps)(MainApp);