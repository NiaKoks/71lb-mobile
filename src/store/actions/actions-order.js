import axios from '../../../axios-orders';
import {ORDER_REQUEST,ORDER_SUCCESS,ORDER_FAILURE,TOGGLE_MODAL} from "./actions-types";
import {initDishes} from "./actions-dishes";

export const orderReq = () =>({type: ORDER_REQUEST});
export const orderSuc = dishes => ({type: ORDER_SUCCESS,dishes});
export const orderFail = error =>({type: ORDER_FAILURE,error});
export const toggleModal =()=>({type:TOGGLE_MODAL});

export const createListOfDishes = ()=>{
    return dispatch =>{
        dispatch(orderReq());
        axios.get('dishes.json').then(response=>{dispatch(orderSuc(response.data));
        },error => dispatch(orderFail(error))
        );
    }
};
export const createOrder =(orderData,history) =>{
    return dispatch=>{
        dispatch (orderReq());
        axios.post('orders.json',orderData).then(response=>{
            dispatch(createListOfDishes());
            dispatch(initDishes());
            history.push('/');
        }, error=>dispatch(orderFail(error)))
    }
};
export const delOrder = (orderData)=>{
    return dispatch =>{
        dispatch(orderReq());
        axios.delete('dishes/'+orderData+'.json').then(response=>{
            dispatch(createListOfDishes());
        }, error => dispatch(orderFail(error))
        );
    }
};