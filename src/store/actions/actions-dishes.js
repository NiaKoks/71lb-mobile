import {ADD, DEL,EDIT, INITDISH} from "./actions-types";

export const addDish = (title,price) =>({type:ADD,title,price});
export const editDish = (title,price,img) =>({type:EDIT,title,price,img});
export const delDish = (title,price) =>({type:DEL,title,price});
export const initDishes = () =>({type:INITDISH});



