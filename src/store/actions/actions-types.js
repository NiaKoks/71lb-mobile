export const ADD ='ADD';
export const DEL ='DEL';

export const INITDISH = 'ITITDISH';
export const POSTORDER = 'POSTORDER';

export const ORDER_REQUEST='ORDER_REQUEST';
export const ORDER_SUCCESS ='ORDER_SUCCESS';
export const ORDER_FAILURE ='ORDER_FAILURE';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';
