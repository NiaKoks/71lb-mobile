import {ADD,DEL,INITDISH,POSTORDER,
        ORDER_REQUEST,ORDER_FAILURE,
        ORDER_SUCCESS,TOGGLE_MODAL}
from "../actions/actions-types";

const initialState ={
    orderedDishes:{},
    totalPrice: 0,
    delivery: 150,
    error: null,
    loader: false,
};

const parsingDishes = (dishes) =>{
    const orderedDishes = {};
    Object.keys(dishes).forEach(id =>{
        orderedDishes[dishes[id].title] = {
            count: 0,
            price:dishes[id].price,
            id
        }
    });
    return orderedDishes;
};

const orderReducer =(state=initialState,action) =>{
    switch (action.type) {
        case ADD:{
            return {...state,
            orderedDishes: {
                ...state.orderedDishes,
                [action.title]:{
                    ...state.orderedDishes[action.title],
                    count: state.orderedDishes[action.title].count +1
                }},
            totalPrice: (state.totalPrice) + parseInt(action.price)
            }
        };
        case DEL:{
            return {...state,
                orderedDishes: {
                    ...state.orderedDishes,
                    [action.title]:{
                        ...state.orderedDishes[action.title],
                        count: state.orderedDishes[action.title].count -1
                    }},
                totalPrice: (state.totalPrice) - parseInt(action.price)
            }};
        case POSTORDER:{
            return {
                ...state,
                orderedDishes: {
                    ...state.orderedDishes,
                    [action.title]: {
                        ...state.orderedDishes[action.title],
                        count : state.orderedDishes[action.title].count - 1
                    }},
                totalPrice: (state.totalPrice) - parseInt(action.price)
            }};
        case ORDER_REQUEST:
            return {...state, loading: true};
        case ORDER_SUCCESS:
            return {...state, loading: false, orderDishes: parseDishes(action.dishes)};
        case ORDER_FAILURE:
            return {...state, loading: false, error: action.error};
        case TOGGLE_MODAL:
            return {...state, modalVisible: !state.modalVisible};
        default:
            return state;
    }
};

export default orderReducer;
