import {ORDER_SUCCESS, ORDER_FAILURE, ORDER_REQUEST, TOGGLE_MODAL} from '../actions/actions-types'
const initialState = {
    dishes: {},
    error: null,
    loading:false,
    modalVisible: false,
    ordered: false,
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_REQUEST: {
            return{
                ...state,
                loading:true,
            }
        };
        case ORDER_SUCCESS: {
            return{
                ...state,
                loading:false,
                dishes: action.dishes,
            }
        };
        case ORDER_FAILURE: {
            return{
                ...state,
                loading:false,
                dishes: action.dishes,
            }
        };
        case TOGGLE_MODAL: {
            return{
                ...state,
                modalVisible: !state.modalVisible
            }
        };
        default: return state;
    }
};

export default dishesReducer;
