import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';
import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {Provider} from 'react-redux'
import dishesReducer from './src/store/reducer/Dishes-reducer';
import orderReducer from './src/store/reducer/Order-reducer';
import MainApp from "./src/containers/MainApp/MainApp";

const rootReducer = combineReducers({
  dishesReducer: dishesReducer,
  orderReducer: orderReducer
});

const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware)
);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MainApp/>
      </Provider>
    );
  }
}


